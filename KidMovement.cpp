#include "KidMovement.h"

void Basic::kid::processMovementInput() {
	this->speed.x = 0; // Remove this if you wish to make players have horizontal inertia. Many other adjustments will be necessary, however.
	if (this->currentGravityPull < this->gravity_pull) this->currentGravityPull = this->gravity_pull;
	if (this->_isJumping) { // If the player is jumping this frame.
		if (this->_newJump) { // If the player initiated a jump this frame.
			if (this->_isAirborne) {
				if (!this->_hasAirJumped) {
					this->speed.y = -this->sJumpSpeed;
					this->_hasAirJumped = true;
					this->currentGravityPull = 0;
				}
			}
			else {
				this->speed.y = -this->fJumpSpeed;
				this->currentGravityPull = 0;
			}
		}
		else { // Lower the effect of gravity if the player is keeping the jump key pressed and he is still ascending.
			if (this->speed.y < 0) {
				this->speed.y += this->currentGravityPull;
			}
		}
	}
	else if (this->_isAirborne) {
		this->speed.y += this->currentGravityPull;
		this->currentGravityPull += this->gravity_pull;
		if (abs(this->speed.y) > this->gravity_maxSpeed) this->speed.y = sgn(this->speed.y) + gravity_maxSpeed;
	}
	else {
		this->currentGravityPull = 0;
		this->speed.y = 0;
		this->_hasAirJumped = false;
	}

	if (this->_isMovingRight) {  // Left/Right movement handled here.
		this->speed.x = 3;
	}
	else if (this->_isMovingLeft) { // In regular fangames, moving right takes priority over moving left.
		this->speed.x = -3;
	}


	// If you wish for you code to be affected by gravity flip, place it below this line.



	int inter;
	switch (this->gravity_direction) { // There is no case 0 or default as if the gravity is normal, the previous lines of code will already work as intended.
	case 1:
		this->speed.y = -this->speed.y;
		break;

	case 2:
		inter = this->speed.x;
		this->speed.x = this->speed.y;
		this->speed.y = inter;

		if (_shouldPlayerSpriteFollowGravity) { // Handling hitbox modification
			this->hitbox.x = this->baseHitbox.y;
			this->hitbox.y = this->baseHitbox.x;
		}
		break;

	case 3:
		inter = this->speed.x;
		this->speed.x = -this->speed.y;
		this->speed.y = -inter;

		if (_shouldPlayerSpriteFollowGravity) { // Handling hitbox modification
			this->hitbox.x = this->baseHitbox.y;
			this->hitbox.y = this->baseHitbox.x;
		}
		break;
	}


	// If you wish to affect the player's movement independently to gravity, do it below this line.


}

void Basic::kid::step() {
	// Check here if the planned movement is possible and if not, selects an appropriate new position.
	this->position.x += this->speed.x;
	this->position.y += this->speed.y;
}



void Basic::kid::assignGraphicTokens(HWND m_Hwnd, ID2D1HwndRenderTarget* m_pRenderTarget) {
	this->m_Hwnd = m_Hwnd;
	this->m_pRenderTarget = m_pRenderTarget;
}

// Will display the player in the future. For now only displays the player's hitbox.
bool Basic::kid::render() {
	RECT windowSize;
	if (GetWindowRect(this->m_Hwnd, &windowSize)) {
		D2D1_SIZE_F rtSize = m_pRenderTarget->GetSize();
		int width = static_cast<int>(rtSize.width);
		int height = static_cast<int>(rtSize.height);
		HRESULT hr;
		ID2D1SolidColorBrush* m_pBrush;
		hr = m_pRenderTarget->CreateSolidColorBrush(
			D2D1::ColorF(D2D1::ColorF::White),
			&m_pBrush
		);
		if (SUCCEEDED(hr)) {
			D2D1_RECT_F rectangle;
			rectangle = D2D1::RectF(this->position.x, this->position.y, this->position.x + this->hitbox.x, this->position.y + this->hitbox.y);
			m_pRenderTarget->DrawRectangle(&rectangle, m_pBrush);
			return true;
		}
	}
	return false;
}


Basic::kid::kid(Basic::saveFormat* save) {
	this->saveRoom = save;

	this->baseHitbox.x = kidHitboxX; // This is what should be modified if you wish to affect the player's original hitbox.
	this->baseHitbox.y = kidHitboxY;
	this->hitbox = baseHitbox; // Represented as a point, as it is only a 2D size, as opposed to a system of points. This is what is used by the game, and is updated depending on baseHitbox. This solely exists to allow dynamic hitboxes that are modified based on the baseHitbox.
	this->position.x = 0;
	this->position.y = 0;
	this->speed.x = 0; // Is the value added to the position at every frame. Multiplying this value by the framerate gives you the kid's speed in px/s.
	this->speed.y = 0;

	this->_isAirborne = false;
	this->_canAirJump = true; // Represents the player's ability to do double jumps. Enabled by default.
	this->_hasAirJumped = false; // Did the player already double jump?
	this->_canInfiniAirJump = false; // Set this to 1 to allow the player to jump indefinitely.

	this->_isJumping = false; // Is the jumping key pressed down?
	this->_newJump = false; // Has the jump key been pressed this frame?
	this->_isMovingRight = false; // Is the player moving right?
	this->_isMovingLeft = false; // Is the player moving left?

	this->gravity_direction = 0;  // For gravity swap gimmicks affecting the player. 0 = normal, 1 = upside-down, 2 = left to right, 3 = right to left.
	this->gravity_maxSpeed = 9; // Represents the maximum fall speed. Default is 9. Modify this value for custom gravity pull.
	this->gravity_pull = 0.4; // Represents gravity's acceleration. Default is 0.4.
	this->fJumpSpeed = 8.5; // Controls the initial velocity of the first jump. Default is 8.5.
	this->sJumpSpeed = 7; // Controls the initial velocity of the first jump. Default is 7.
	this->_shouldPlayerHitboxFollowGravity = true; // Should the player's hitbox rotates alongside gravity ? True by default.
	this->currentGravityPull = 0.4; // Represents how much gravity will pull the player in the next frame, in pixels.

	this->_drawBow = false; // Should the small red bow be drawn on the head of the player?
	this->_isLookingRight = false; // Should the Kid's image look right? Looks left by default (if false).
	this->animationSpeed = 0.2; // "Speed" at which the player's animation should render. Default is 0.2.
	this->animationCounter = 0; // Changes the image of the player when it reaches 1 to animate it.
	this->_shouldDisplay = true;  // Used to make the player disappear when he dies, while still keeping his death location as opposed to simply remove the kid entirely.
	this->_shouldPlayerSpriteFollowGravity = true; // Should the sprite of the player tilt to the side to follow along gravity? Changing this also rotates the player's hitbox alongside it by default.
}