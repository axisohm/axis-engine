# Axis Engine
**Because you deserve the best. At the cost of your sanity, that is.**

**DISCLAIMER : This project is currently in a very early state. As such, consider the things listed below as an objective rather than actual features.**

If you want to make a fangame that will have the ability to behave completely differently that usual fangames, with custom minigames, custom features, or even pseudo-3D, this engine is made for you !

Any question/problem, or if you simply want to reach out to me, here's my Discord : **AxisOhm#5264**. 
If many users end up using this engine, I'll make a Discord server. But I mean, uh, I really doubt it will ever be necessary.

Once finished, I'll make a "simple" tutorial to use this engine, so bear with me while it's in this development state. And a tool to edit rooms easily, because I doubt anyone wants to make a needle game while seeing only their code until they compile and run it.

I'll also probably make some proper documentation here once done, but as it is, I believe that I commented the code enough except for the rendering part. If some comments are obscure, please feel free to contact me. This project originated from another project that I made for myself, so the old comments there might not be appropriated or plain obscure. I'll review those soon.
### Pros :
- **C++ based engine :** Your game will be the fastest and best optimized game you've ever seen !
- **Supports basically anything :** You think of something that would be cool in your fangame? Altered and changing hitboxes, skins, or an actually randomized mine sweeper like in "I Wanna Kill the Kamillia 3's Kamilia Area"? Well now you can do it to any extent. Like, you can legitimately open new windows, make your fangame render on top of the player's desktop, and so much more !
- **Start immediately with a working exemple game. :** Skip having to learn how the engine works entirely before being able to test your game. You can directly modify the provided exemple to progressively transform it into your game.

### Cons :
- **GMS (or any other mainstream engine, really) will most likely be easier to use, and has a bigger community of users that can help you.** I really want this to stand out.
- **No visual interface :** You won't be able to see how your game looks until you start the game, which could slow the development process. A workaround to easily make rooms is planned, but not prioritized currently as the engine itself isn't finished yet.

##### To Do :
- **Liquids and vines :** All elements that affect how the player movements work.
- **Slopes :** If the player walks along a 1x1px slope, he will glide alongside it.
- **Kill type :** All elements will have the option to kill the player upon contact.
- **Vanilla Textures :** Import the default textures used commonly in most fangames into the engine.
- **Vanilla Sound :** Import the default sounds used commonly in most fangames into the engine.
- **AppleSystems :** A class that regroups elements to easily modify their position relative to each others. Wish to make a group of apples or bullets form a 3D-sphere that gyrates as you want? This is the tool that you need !
- **Element Display Optimisation :** If elements go outside the displayed area, they remain loaded in the memory, but aren't displayed anymore. Possibility to also unload them entirely if the user finds it fitting.
- **Cleaner Gravity Handling :** This would allow the players to be rotated in any direction, thus allowing smooth gravity changes, or maybe some specific rooms/gimmicks that use a tilted setup.
- **Moving Blocks and Platforms :** Ways to transport the player across the screen on platforms will be available.