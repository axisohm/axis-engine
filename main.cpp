// Code metrics : Ctrl + Shift + F; [^\n\s]\r\n
#pragma once
#include "Window.h"

using namespace std;


int WINAPI WinMain(HINSTANCE /* hInstance */, HINSTANCE /* hPrevInstance */, LPSTR /* lpCmdLine */, int /* nCmdShow */) {
	// Use HeapSetInformation to specify that the process should
	// terminate if the heap manager detects an error in any heap used
	// by the process.
	// The return value is ignored, because we want to continue running in the
	// unlikely event that HeapSetInformation fails.
	HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);

	if (SUCCEEDED(CoInitialize(NULL))) {
		{
			Basic::Application app;
			if (SUCCEEDED(app.Initialize("Axis Engine", "Axis Engine", InitialWindowSizeX, InitialWindowSizeY))) {
				app.show();
				while (1) {
					// app.writeTextOnScreenLoopWithWindowHandle("Heh"); // If you need to test this app's most basic features, run this function.
					app.windowDisplay();
				}
			}
		} CoUninitialize();
	}
	return 0;
}