#pragma once
//#include "JinkoDependencies.h"
#include "Basic.h"

class Basic::vector {
public:
	float x;
	float y;

	float getNorm();
	float vectorProduct(Basic::vector);
	float getAngle(Basic::vector);

	bool operator==(Basic::vector const&);
	bool operator==(Basic::point const&);
	Basic::vector operator+(Basic::vector const&);
	Basic::vector operator-(Basic::vector const&);
	Basic::point operator+(Basic::point const&);
	Basic::point operator-(Basic::point const&);

	void round();

	vector();
	vector(float, float);
	vector(point);
};

class Basic::point {
public:
	float x;
	float y;

	bool operator==(Basic::point const&);
	Basic::point operator+(Basic::point const&);
	Basic::point operator-(Basic::point const&);

	Basic::point operator+(Basic::vector const&);
	Basic::point operator-(Basic::vector const&);

	void round();

	point();
	point(float, float = 0);
};

class Basic::line {
public:
	Basic::point A;
	Basic::point B;

	bool isOverlapping(Basic::point);
	bool isOverlapping(float, float);

	line();
	line(point, point);
	line(float, float, float, float);
};

class Basic::triangle {
public:
	Basic::point A;
	Basic::point B;
	Basic::point C;

	bool isOverlapping(float, float);
	bool isOverlapping(float, float, float, float);
	bool isOverlapping(Basic::point);
	bool isOverlapping(Basic::point, Basic::point);
	bool isOverlapping(Basic::line);

	triangle();
	triangle(point, point, point);
};

class Basic::quad {
public:
	Basic::point A;
	Basic::point B;
	Basic::point C;
	Basic::point D;

	bool isOverlapping(float, float);
	bool isOverlapping(Basic::point);
	bool isOverlapping(Basic::line);
	bool isOverlapping(Basic::triangle);

	bool isOverlappingPartially(Basic::line);
	bool isOverlappingPartially(Basic::triangle);

	quad();
	quad(point, point, point, point);
};

// Use this rather than Basic::quad for better performances if possible. Will represent a square with A and B being opposite points of it, and its vertices being colinear to the X and Y axis.
class Basic::square {
public:
	Basic::point A;
	Basic::point B;

	bool isOverlapping(float, float);
	bool isOverlapping(Basic::point);
	bool isOverlapping(Basic::line);
	bool isOverlapping(Basic::triangle);

	bool isOverlappingPartially(Basic::line);
	bool isOverlappingPartially(Basic::triangle);

	square();
	square(point, point);
};