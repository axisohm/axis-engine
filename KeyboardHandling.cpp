#include "KeyboardHandling.h"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * inputs * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
//Returns 0 if inactive, 1 if newly inactive, 2 if active, 3 if newly active.
int Basic::input::getInputState() {
	if (this->wasActive == false) {
		if (this->isActive == false) {
			return 0;
		}
		else {
			return 3;
		}
	}
	else {
		if (this->isActive == false) {
			return 1;
		}
		else {
			return 2;
		}
	}
}

Basic::input::input() {
	this->key = 0x97; // 0x97 is an unassigned keyboard key. As such, it is never represented on a regular keyboard and serves no purpose.
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * mouseInputs * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void Basic::mouseInputs::update() {
	for (int i = 0; i < AmountMouseKeys; i++) {
		SHORT test = GetKeyState(this->keys[i].key);
		this->keys[i].wasActive = this->keys[i].isActive;
		if (test % (2 ^ 15) == 1) {
			this->keys[i].isActive = true;
		}
		else {
			this->keys[i].isActive = false;
		}
	}
}

int Basic::mouseInputs::getInputState(int which) {
	if (this->keys[which].wasActive == false) {
		if (this->keys[which].isActive == false) {
			return 0;
		}
		else {
			return 3;
		}
	}
	else {
		if (this->keys[which].isActive == false) {
			return 1;
		}
		else {
			return 2;
		}
	}
}

Basic::mouseInputs::mouseInputs() {
	this->keys[0].key = VK_LBUTTON;
	this->keys[1].key = VK_MBUTTON;
	this->keys[2].key = VK_RBUTTON;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * inputBoard * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void Basic::inputBoard::update() {
	for (int i = 0; i < AmountKeyboardkeys; i++) {
		if (this->keys[i].key != 0x97) {
			SHORT test = GetKeyState(this->keys[i].key);
			this->keys[i].wasActive = this->keys[i].isActive;
			if (test < 0) {
				this->keys[i].isActive = true;
			}
			else {
				this->keys[i].isActive = false;
			}
		}
	}
	this->mouse.update();
}

int Basic::inputBoard::getKeyboardInput(int key) {
	return this->keys[key].getInputState();
}

int Basic::inputBoard::getMouseInput(int button) {
	return this->mouse.getInputState(button);
}

Basic::inputBoard::inputBoard() { // Set here the basic inputs
	this->keys[Basic_Keyboard_Left].key = VK_LEFT;
	this->keys[Basic_Keyboard_Right].key = VK_RIGHT;
	this->keys[Basic_Keyboard_Jump].key = VK_SHIFT;
	this->keys[Basic_Keyboard_Shoot].key = AZERTY_W;
	this->keys[Basic_Keyboard_FPSdisplay].key = VK_F1;
}
