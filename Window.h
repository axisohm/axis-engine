#pragma once
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "MSVCRTD.lib")
#pragma comment(lib, "Dwrite")

// Windows Header Files:
#include <windows.h>

// C RunTime Header Files:
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <math.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include <string>

#include <chrono>
#include <ctime>

#include <iostream>
#include <filesystem>
#include <fstream>

// Engine framework :
#include "BasicDependencies.h"
#include "Config.h"

#define maxElementsInMenu 100 // Represents how many UIelement there are in each UIcollection.
#define maxCharaInButton 50 //TODO I, uh, don't remember why I made this. I'll look it up at some point.


//Image ressources :
bool loadBitmaps(HWND, ID2D1HwndRenderTarget*);
HRESULT loadABitmap(HWND, ID2D1HwndRenderTarget*, int, std::string); //string should be the path, i.e. : "./Ressources/Fight/RPG1_Assets/HPbarPercentage0.png"
HRESULT loadABitmapFromResource(HWND, ID2D1HwndRenderTarget*, int, int);
void unloadBitmaps();


// SafeRelease : Use this whenever you finish using a Direct2D feature, otherwise you will generate memory leaks.
template<class Interface>
inline void SafeRelease(Interface** ppInterfaceToRelease) {
    if (*ppInterfaceToRelease != NULL)
    {
        (*ppInterfaceToRelease)->Release();

        (*ppInterfaceToRelease) = NULL;
    }
}

// Debug macro :
#ifndef Assert
#if defined( DEBUG ) || defined( _DEBUG )
#define Assert(b) do {if (!(b)) {OutputDebugStringA("Assert: " #b "\n");}} while(0)
#else
#define Assert(b)
#endif //DEBUG || _DEBUG
#endif

// Debug macro :
#ifndef HINST_THISCOMPONENT
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)
#endif



class Basic::Application {
public:
    Application();
    ~Application();

    // Register the window class and call methods for instantiating drawing resources
    HRESULT Initialize(std::string, std::string, int , int);
    HRESULT InitializeLayered(std::string, std::string, int, int);

    void resizeRenderTargetToWindowSize();
    void ResizeWindow(int x, int y);
    void ResizeAll(int x, int y);

    void actualizeWindow(UIcollection*);

    // Here is a test functions (to run in main.cpp instead of the main loop) :
    void writeTextOnScreenLoopWithWindowHandle(std::string text);
    // This is used in the test function.
    bool writeTextOnScreen(std::string);

    // Base game loop :
    void windowDisplay();

    // Other Functions :
    void show();
    void hide();

    void drawLine(ID2D1HwndRenderTarget*, int, int, int, int, int, int);
    void drawRectangle(ID2D1HwndRenderTarget*, int, int, int, int, int);
    

private:
    // Initialize device-independent resources.
    HRESULT CreateDeviceIndependentResources();

    // Initialize device-dependent resources.
    HRESULT CreateDeviceResources();

    // Release device-dependent resource.
    void DiscardDeviceResources();

    // Draw content.
    HRESULT OnRender();

    // Resize the render target.
    void OnResize(
        UINT width,
        UINT height
    );

    // The windows procedure.
    static LRESULT CALLBACK WndProc(
        HWND hWnd,
        UINT message,
        WPARAM wParam,
        LPARAM lParam
    );

    Basic::inputBoard* Keyboard;

    // Layered window stuff
    HRESULT CreateDeviceResourcesLayered();
    void DiscardDeviceResourcesLayered();

    HWND m_hwnd;
    ID2D1Factory* m_pDirect2dFactory;
    IDWriteFactory* m_pD2dWriteFactory;
    IDWriteTextFormat* MainMenuMainButtonsTextFormat;
    IDWriteTextFormat* m_pNotificationTextFormat;
    ID2D1HwndRenderTarget* m_pRenderTarget;
    ID2D1SolidColorBrush* m_pLightSlateGrayBrush;
    ID2D1SolidColorBrush* m_pCornflowerBlueBrush;
    ID2D1SolidColorBrush* m_pBgGreyBrush;

    UIcollection* windowHandle;
    UIcollection* settingsMenu;
    DWORD windowStyle;
};


class Basic::UIelement {
private:
    unsigned int alignement = 0;
    /*  0x00 = None,
        0x01 = up,
        0x02 = down,
        0x04 = left,
        0x08 = right,
        0x10 = centered.
        /!\ These are mostly unused.
    */
    POINT position = { 0, 0 }; // Relative to alignment.
    POINT currentPosition; // Is updated everytimes the element is drawn
    POINT size;

    std::string text; // To write somewhere, depending on the style.
    unsigned int style;
    unsigned int type;

    DWORD windowStyle;
    /* Note : Some "Type" values are skipped as they were previously used in the project this originated from. Use them at your own discretion.
    Type :
        0 : Basic functions
            Styles :
                0 : Don't draw anything, but still generates a rectangular collision section, depending on the size and the position. Use this for triggers for exemple.
                1 : Basic button
                2 : Exit button (cannot be moved or changed. Only one displayed element of this type is useful)
                3 : Popup text window
                4 : Popup close button
                5 : Draws a text at target location of desired size (size), font (fontName) and color (colorValue).
                6 : Draws a text at target location of desired size (size), font (fontName) and color (colorValue) progressively. Uses value1, incrementor1 and clockValue1 to determine what to write. Uses (_shouldWrite) to determine if it should continue writing.
                7 : Draws a pixelized screen of (colorValue) color with each pixel having a random opacity from 0 to (opacityFactor1). Uses (value1) to determine the size of the pixels.
                8 : Displays a text at the target location (position) on the screen using this->text. Requires external handling for counting and actually displaying FPS.

        1 : Image Handler
            Styles :
                0 : Draws an image at target location (position) and generate its collision zone.
                1 : Draws the image at target location (position) from ressources

        4 : RenderTarget Transformation effects
            Note : Requires to have reset the render target at least once each frames.
            Styles :
                0 : Reset the RenderTarget to normal.
                1 : Cylinder effect.
    */
    bool _isVoid;

    std::chrono::system_clock::time_point clockValue1;
    std::chrono::system_clock::time_point clockValue2;
    std::chrono::system_clock::time_point timeOffset;
    bool _shouldWrite;


    // Adress of a file to handle (like the image path).
    std::string adress;
    DWORD handleElement;
    ID2D1Bitmap* image = nullptr;
    ID2D1Factory* m_pDirect2dFactory;
    IDWriteFactory* m_pD2dWriteFactory;
    IWICImagingFactory* pIWICFactory;
    IDWriteTextFormat* MainMenuMainButtonsTextFormat;
    IDWriteTextFormat* textFormat;
public:
    bool bool1 = false;
    bool bool2 = false;
    float opacityFactor1 = 1;
    
    bool _jobDone = false;
    int characterPosition;

    int value1; //Used in "UIcollection.poptext()" to move the text displayed. Also in other functions in UIelement.
    int value2;
    int value3;
    int value4;
    int value5;
    float fvalue1;
    int incrementor1;
    wchar_t fontName = *L"Gabriola";
    unsigned int colorValue;


    void resize(POINT);
    void resize(int, int);
    std::string getText();
    unsigned int getStyle();
    unsigned int getAlignement();
    bool get_isVoid();

    void reText(std::string);
    void reStyle(unsigned int);
    void reType(unsigned int);
    void re_isVoid(bool);

    void startWriting();
    void stopWriting();

    bool updatePosition(HWND);
    bool drawElement(HWND, ID2D1HwndRenderTarget*);

    void setImagePath(std::string);
    std::string getImagePath();

    void setNewRotationValueMenuRPG1(int);

    void setHandleElement(DWORD);

    void rePosition(POINT, unsigned int = 0);
    void rePosition(int, int, unsigned int = 0);
    void reRelativePosition(POINT);
    void reRelativePosition(int=0, int=0);

    RECT getArea();

    void hide();
    void show();
    bool _isDisplayed();

    bool check_isHovered(HWND, ID2D1HwndRenderTarget*, POINT);

    void init();

    bool _mustDisplay; // Wether or not the element must be drawn
    RECT activeArea;

    HRESULT CreateDeviceIndependentResources();


    void generateRectangleCollisionSection(int, int, int, int);


    // BASIC FUNCTIONS :
        // Check above for full descriptions. Type : 0, Style   --------------------------->     Below
        void generateRectangleCollisionSection();                                               // 0
        bool drawBasicButton(HWND, ID2D1HwndRenderTarget*);
        bool drawExitButton(HWND, ID2D1HwndRenderTarget*);
        bool drawPopupTextWindow(HWND, ID2D1HwndRenderTarget*);
        bool drawPopupCloseButton(HWND, ID2D1HwndRenderTarget*);
        bool drawText(HWND, ID2D1HwndRenderTarget*);                                            // 5
        bool drawTextProgressively(HWND, ID2D1HwndRenderTarget*);
        bool drawPixelizedBlackFrame(HWND, ID2D1HwndRenderTarget*);
        bool displayFPSCounter(HWND, ID2D1HwndRenderTarget*);

    // IMAGE HANDLER :
        // Check above for full descriptions. Type : 1, Style   --------------------------->     Below
        bool drawImageFromHandle(HWND, ID2D1HwndRenderTarget*);                                 // 0
        bool drawImage(HWND, ID2D1HwndRenderTarget*);


    // RENDERTARGET TRANSFORMATION EFFECTS FUNCTIONS :
        // Check above for full descriptions. Type : 4, Style   --------------------------->     Below
        bool renderTargetTransformationReset(HWND, ID2D1HwndRenderTarget*);                     // 0
        bool renderTargetTransformationCylinderEffect(HWND, ID2D1HwndRenderTarget*);
    // END OF RENDERTARGET TRANSFORMATION EFFECTS FUNCTIONS FUNCTIONS

    void affectWindowStyle(DWORD);

    UIcollection* owner;

    UIelement();
    ~UIelement();
    UIelement(POINT, unsigned int = 0);
};


class Basic::UIcollection {
private:
    UIelement elements[maxElementsInMenu];
    HWND m_Hwnd;
    ID2D1SolidColorBrush* m_pBgBrush;
    ID2D1HwndRenderTarget* m_pRenderTarget;
    bool _mustDisplay = true;
    bool _drawBg;

    DWORD windowStyle;

public:

    int _isHovered[maxElementsInMenu];
    bool displayElements(HWND);
    UIelement getElement(int);
    UIelement* getElementPointer(int);
    void addElement(UIelement);
    void insertElement(UIelement, int);
    void removeElement(int);
    bool drawElements(HWND, ID2D1HwndRenderTarget*);
    bool checkIfHovered(POINT);
    void affectWindow(HWND);

    void setBgBrush(ID2D1SolidColorBrush*);

    void hide();
    void show();
    bool isDisplayed();
    void _doDrawBg(bool);

    void refreshElementsOwner();

    void init(ID2D1HwndRenderTarget*);
    void init(ID2D1HwndRenderTarget*, HWND);

    void popText(std::string);
    void popMoreText(std::string);

    void affectWindowStyle(DWORD);

    void moveRelativePosition(POINT);
    void moveRelativePosition(int, int);

    UIcollection(HWND, ID2D1HwndRenderTarget*, DWORD);
    UIcollection(HWND, ID2D1HwndRenderTarget*, ID2D1SolidColorBrush*, DWORD);
    UIcollection();
    ~UIcollection();
};
