#pragma once
#include "BasicDependencies.h"
#include "DefaultRooms.h"

// I tried to put most of the game's configuration constants here. Modify this at your convenience.

#define StartingRoom exempleRoom // What should be the first room of your game.



#define InitialWindowSizeX 800 // A normal room size is 800x608. You may modify this value as you wish, however.
#define InitialWindowSizeY 608

#define FPSlimitation 1 // Limits the framerate of the game to this value. Use 0 for unlimited FPS. Caution : Fangames by default relies on the framerate as it should rely on time. If your game is running at 100fps for exemple, it will feel as if its speed is doubled when you play it, as all movements rely on a "50fps game".
#define updateRateFPSCounter 16 // Amount of frames to use as samples to calculate the FPS rate. Higher values gives higher accuracy over long periods of time, but will be less sensible to lag spikes and will take longer to process.

#define kidHitboxX 11 // Set as "1" for dotkid hitbox for exemple. Default is 11.
#define kidHitboxY 21 // Same as above. Default is 21.