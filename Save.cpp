#include "Save.h"

bool Basic::saveFormat::exportSaveFile() {
	_wmkdir(L"./Data"); // Creates the folder "Data" if it doesn't exist. 
	std::ofstream file(savePath, std::ios::trunc); //ios::trunc is used to erase the previous content before editing the file.

	if (file.is_open()) {
		file << "saveRoom=" << this->saveRoom << std::endl;
		file << "savePositionX=" << this->savePosition.x << std::endl;
		file << "savePositionY=" << this->savePosition.y << std::endl;
		file << "achievements=" << this->achievements << std::endl;

		file.close();
	}
	return false;
}
bool Basic::saveFormat::loadSaveFile() {
	std::ifstream file(savePath); //ios::trunc is used to erase the previous content before editing the file.

	if (file.is_open()) {
		std::string save(std::istreambuf_iterator<char>(file), {});
		file.close();
		std::string iterator = outStat("saveRoom=", save);
		this->saveRoom = stringToInt(iterator);
		iterator = outStat("savePositionX=", save);
		this->savePosition.x = stringToInt(iterator);
		iterator = outStat("savePositionY=", save);
		this->savePosition.y = stringToInt(iterator);
		iterator = outStat("achievements=", save);
		this->savePosition.y = stringToInt(iterator);
	}
	return false;
}

Basic::saveFormat::saveFormat() {
	this->saveRoom = 0;
	this->savePosition.x = 0;
	this->savePosition.y = 0;
	this->achievements = 0;
}