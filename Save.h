#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <Windows.h>
#include <conio.h>
#include <string>
#include <fstream>
#include <iomanip>


#include "BasicDependencies.h"

#define savePath L"./Data/saveData"

// If you need to save additional things, like custom achivements, or any other thing, you can add them in the Basic::saveFormat class.
// You'll then need to modify the "Basic::saveFormat::save()", "Basic::saveFormat::exportSaveFile()" and "Basic::saveFormat::loadSaveFile()" functions in order to have it save and load it correctly.

class Basic::saveFormat {
public:
	int saveRoom;
	Basic::point savePosition;
	int achievements; // Advice : use a bit-wise representation for each achievements in order to represent many in this variable.

	bool exportSaveFile();
	bool loadSaveFile();

	bool save(int, Basic::point, int);

	saveFormat();
};