#include "geometry.h"



// * * * * * * * * * * * * * * VECTOR * * * * * * * * * * * * * * //

float Basic::vector::getNorm() {
	return sqrt(pow(this->x, 2) + pow(this->y, 2));
}
float Basic::vector::vectorProduct(Basic::vector A) {
	return ((this->x * A.x) + (this->y * A.y));
}
float Basic::vector::getAngle(Basic::vector A) {
	if ((this->getNorm() == 0) || (A.getNorm() == 0)) return 0;
	else return acos(((this->x * A.x) + (this->y * A.y)) / (this->getNorm() * A.getNorm()));
}

bool Basic::vector::operator==(Basic::vector const& A) {
	return ((this->x == A.x) && (this->y == A.y));
}
bool Basic::vector::operator==(Basic::point const& A) {
	return ((this->x == A.x) && (this->y == A.y));
}
Basic::vector Basic::vector::operator+(Basic::vector const& A) {
	Basic::vector a(this->x + A.x, this->y + A.y);
	return a;
}
Basic::vector Basic::vector::operator-(Basic::vector const& A) {
	Basic::vector a(this->x - A.x, this->y - A.y);
	return a;
}
Basic::point Basic::vector::operator+(Basic::point const& A) {
	Basic::point a(this->x + A.x, this->y + A.y);
	return a;
}
Basic::point Basic::vector::operator-(Basic::point const& A) {
	Basic::point a(this->x + A.x, this->y + A.y);
	return a;
}


void Basic::vector::round() {
	this->x = roundf(this->x);
	this->y = roundf(this->y);
}


Basic::vector::vector() {
	this->x = 0;
	this->y = 0;
}
Basic::vector::vector(float x, float y = 0) {
	this->x = x;
	this->y = y;
}
Basic::vector::vector(point A) {
	this->x = A.x;
	this->y = A.y;
}

// * * * * * * * * * * * * * * POINT * * * * * * * * * * * * * * //

bool Basic::point::operator==(Basic::point const& A) {
	if ((this->x == A.x) && (this->y == A.y)) return true;
	else return false;
}
Basic::point Basic::point::operator+(Basic::point const& A) {
	Basic::point a(this->x + A.x, this->y + A.y);
	return a;
}
Basic::point Basic::point::operator-(Basic::point const& A) {
	Basic::point a(this->x - A.x, this->y - A.y);
	return a;
}
Basic::point Basic::point::operator+(Basic::vector const& A) {
	Basic::point a(this->x + A.x, this->y + A.y);
	return a;
}
Basic::point Basic::point::operator-(Basic::vector const& A) {
	Basic::point a(this->x - A.x, this->y - A.y);
	return a;
}

void Basic::point::round() {
	this->x = roundf(this->x);
	this->y = roundf(this->y);
}

Basic::point::point() {
	this->x = 0;
	this->y = 0;
}
Basic::point::point(float x, float y) {
	this->x = x;
	this->y = y;
}


// * * * * * * * * * * * * * * LINE * * * * * * * * * * * * * * //

bool Basic::line::isOverlapping(Basic::point X) {
	Basic::vector V(this->B.x - this->A.x, this->B.y - A.y);
	if ((V.x == 0) && (V.y == 0)) return (this->A == X);
	else if (V.x == 0) {
		return (((V.y + this->A.y >= X.y) && (this->A.y <= X.y)) || ((V.y + this->A.y <= X.y) && (this->A.y >= X.y)));
	}
	else if (V.y == 0) {
		return (((V.x + this->A.x >= X.x) && (this->A.x <= X.x)) || ((V.x + this->A.x <= X.x) && (this->A.x >= X.x)));
	}
	else {
		float alpha1 = (float)(X.x / V.x) - (float)(A.x / V.x);
		float alpha2 = (float)(X.y / V.y) - (float)(A.y / V.y);
		return (alpha1 == alpha2);
	}
}
bool Basic::line::isOverlapping(float x, float y = 0) {
	point a(x, y);
	return this->isOverlapping(a);
}

Basic::line::line() {
	this->A.x = 0;
	this->A.y = 0;
	this->B.x = 0;
	this->B.y = 0;
}
Basic::line::line(float x1, float y1, float x2, float y2) {
	this->A.x = x1;
	this->A.y = y1;
	this->B.x = x2;
	this->B.y = y2;
}
Basic::line::line(Basic::point A, Basic::point B) {
	this->A = A;
	this->B = B;
}


// * * * * * * * * * * * * * * TRIANGLE * * * * * * * * * * * * * * //

// PROBABLY NOT WORKING -- NEEDS TESTING
bool Basic::triangle::isOverlapping(Basic::point X) {
	vector v1(this->B - this->A);
	vector v2(this->C - this->A);
	vector v3(X - this->A);

	float a = v1.getAngle(v2);
	float b = v1.getAngle(v3);

	if (b <= a) {
		a = v2.getAngle(v1);
		b = v2.getAngle(v3);
		if (b <= a) {
			v1 = this->A - this->B;
			v2 = this->C - this->B;
			v3 = X - this->B;

			a = v1.getAngle(v2);
			b = v1.getAngle(v3);
			if (b <= a) {
				a = v2.getAngle(v1);
				b = v2.getAngle(v3);
				if (b <= a) 
					return true;
			}
		}
	}
	return false;
}
bool Basic::triangle::isOverlapping(float x, float y) {
	Basic::point A(x, y);
	return this->isOverlapping(A);
}
bool Basic::triangle::isOverlapping(float x1, float y1, float x2, float y2) {
	Basic::point A(x1, y1);
	Basic::point B(x2, y2);
	return this->isOverlapping(A, B);
}
bool Basic::triangle::isOverlapping(Basic::point A, Basic::point B) {
	return (this->isOverlapping(A) && this->isOverlapping(B));
}
bool Basic::triangle::isOverlapping(Basic::line L) {
	return (this->isOverlapping(L.A) && this->isOverlapping(L.B));
}

Basic::triangle::triangle() {
	this->A.x = 0;
	this->A.y = 0;
	this->B.x = 0;
	this->B.y = 0;
	this->C.x = 0;
	this->C.y = 0;
}
Basic::triangle::triangle(point A, point B, point C) {
	this->A = A;
	this->B = B;
	this->C = C;
}


// * * * * * * * * * * * * * * QUAD * * * * * * * * * * * * * * * //


bool Basic::quad::isOverlapping(float x, float y) {
	Basic::triangle pre(this->A, this->B, this->C);
	Basic::triangle deu(this->A, this->C, this->D);
	return (pre.isOverlapping(x, y) || deu.isOverlapping(x, y));
}
bool Basic::quad::isOverlapping(Basic::point X) {
	return this->isOverlapping(X.x, X.y);
}
bool Basic::quad::isOverlapping(Basic::line L) {
	return (this->isOverlapping(L.A) && this->isOverlapping(L.B));
}
bool Basic::quad::isOverlapping(Basic::triangle T) {
	return (this->isOverlapping(T.A) && this->isOverlapping(T.B) && this->isOverlapping(T.C));
}

bool Basic::quad::isOverlappingPartially(Basic::line L) {
	return (this->isOverlapping(L.A) || this->isOverlapping(L.B));
}
bool Basic::quad::isOverlappingPartially(Basic::triangle T) {
	return (this->isOverlapping(T.A) || this->isOverlapping(T.B) || this->isOverlapping(T.C));
}


Basic::quad::quad() {
	this->A.x = 0;
	this->A.y = 0;
	this->B.x = 0;
	this->B.y = 0;
	this->C.x = 0;
	this->C.y = 0;
	this->D.x = 0;
	this->D.y = 0;
}
Basic::quad::quad(point A, point B, point C, point D) {
	this->A = A;
	this->B = B;
	this->C = C;
	this->D = D;
}


// * * * * * * * * * * * * * * SQUARE * * * * * * * * * * * * * * * //


bool Basic::square::isOverlapping(float x, float y) {
	if (((x >= A.x && x <= B.x) || (x <= A.x && x >= B.x)) && ((y >= A.y && y <= B.y) || (y <= A.y && y >= B.y))) return true;
	return false;
}
bool Basic::square::isOverlapping(Basic::point X) {
	return this->isOverlapping(X.x, X.y);
}
bool Basic::square::isOverlapping(Basic::line L) {
	return (this->isOverlapping(L.A) && this->isOverlapping(L.B));
}
bool Basic::square::isOverlapping(Basic::triangle T) {
	return (this->isOverlapping(T.A) && this->isOverlapping(T.B) && this->isOverlapping(T.C));
}

bool Basic::square::isOverlappingPartially(Basic::line L) {
	return (this->isOverlapping(L.A) || this->isOverlapping(L.B));
}
bool Basic::square::isOverlappingPartially(Basic::triangle T) {
	return (this->isOverlapping(T.A) || this->isOverlapping(T.B) || this->isOverlapping(T.C));
}


Basic::square::square() {
	this->A.x = 0;
	this->A.y = 0;
	this->B.x = 0;
	this->B.y = 0;
}
Basic::square::square(point A, point B) {
	this->A = A;
	this->B = B;
}