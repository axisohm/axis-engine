#pragma once
#include "BasicDependencies.h"
#include "Config.h"
#include "Window.h"

class Basic::kid {
public:
	Basic::point hitbox; // Represented as a point, as it is only a 2D size, as opposed to a system of points. This is what is used by the game, and is updated depending on baseHitbox. This solely exists to allow dynamic hitboxes that are modified based on the baseHitbox.
	Basic::point baseHitbox; // This is what should be modified if you wish to affect the player's original hitbox.

	Basic::point position;
	Basic::point speed; // Is the value added to the position at every frame. Multiplying this value by 50 gives you the kid's speed in px/s.

	// Player status :
	bool _isAirborne;
	bool _canAirJump; // Represents the player's ability to do double jumps. Enabled by default.
	bool _hasAirJumped; // Did the player already double jump?
	bool _canInfiniAirJump; // Set this to 1 to allow the player to jump indefinitely.
	bool _isOnPlatform; // Represents if the player is on a platform in order to allow the player to move alongside it.

	// Keys stuff (to be bound in the main loop):
	bool _isJumping; // Is the jumping key pressed down?
	bool _newJump; // Has the jump key been pressed this frame?
	bool _isMovingRight; // Is the player moving right?
	bool _isMovingLeft;	// Is the player moving left?

	// Jump and gravity processing :
	int gravity_direction; // For gravity swap gimmicks affecting the player. 0 = normal, 1 = upside-down, 2 = left to right, 3 = right to left.
	double gravity_maxSpeed; // Represents the maximum fall speed. Default is 9. Also affects maximum ascend speed, following regular I wanna engines.
	double gravity_pull; // Represents gravity's base acceleration. Default is 0.4.
	double fJumpSpeed; // Controls the initial velocity of the first jump. Default is 8.5.
	double sJumpSpeed; // Controls the initial velocity of the first jump. Default is 7.
	bool _shouldPlayerHitboxFollowGravity; // Should the player's hitbox rotates alongside gravity ? True by default.
	double currentGravityPull; // Represents how much gravity will pull the player in the next frame, in pixels.

	//Visual stuff
	bool _drawBow; // Should the small red bow be drawn on the head of the player?
	bool _isLookingRight; // Should the Kid's image look right? Looks left by default (if false).
	double animationSpeed; // "Speed" at which the player's animation should render. Default is 0.2.
	double animationCounter; // Changes the image of the player when it reaches 1 to animate it.
	bool _shouldDisplay; // Used to make the player disappear when he dies, while still keeping his death location as opposed to simply remove the kid entirely.
	bool _shouldPlayerSpriteFollowGravity; // Should the sprite of the player tilt to the side to follow along gravity? True by default.
	int skin; // Represents the appearence of the player. 0 displays only the hitbox as a white rectangle. 1 displays the original Kid sprites.

	void processMovementInput(); // Called at every loop to process the inputs used.
	void step(); // Called at every "frame". Makes the kid actually move.


	Basic::saveFormat* saveRoom;

	// Rendering functions :
	HWND m_Hwnd;
	ID2D1HwndRenderTarget* m_pRenderTarget;

	void assignGraphicTokens(HWND,ID2D1HwndRenderTarget*);	// MUST be called and assigned correctly before using kid::display().
	bool render();

	kid(Basic::saveFormat*);
private:
};