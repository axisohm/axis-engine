#pragma once
// Some libraries won't work unless specified through these commands, even if included.
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "MSVCRTD.lib")
#pragma comment(lib, "Dwrite")
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files:
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <math.h>

#include <d2d1.h>
#include <d2d1_1.h>
#include <d2d1helper.h>
#include <d2d1effects.h>
#include <dwrite.h>
#include <wincodec.h>

#include <string>

#include <chrono>
#include <ctime>

#include <iostream>
#include <filesystem>
#include <fstream>
#include <vcruntime.h>

// Assets
//#include "resource.h"

// To be updated in order to add new images.
#define bitmapAmounts 1

namespace Basic {
	// KeyboardDisposition.h
	class input;
	class mouseInputs;
	class inputBoard;

	// Window.h
	class Application; // That's your game, as a whole. This class handles displaying an app on your computer, and contains your main game loop by default.
	class UIcollection; // Represents a group of elements represented on the screen. Useful for things like menus or HUD, as you can choose to show, hide, or modify all elements in it easily.
	class UIelement; // Represents any element represented on the screen.
	extern ID2D1Bitmap* basicBitmaps[bitmapAmounts]; // Contains all base images. Yeah it's scary...

	// geometry.h
	class vector;
	class point;
	class line;
	class triangle;
	class quad;
	class square;

	// KeyboardHandling.h
	class input;
	class mouseInputs;
	class inputBoard;

	// Save.h
	class saveFormat;

	// KidMovement.h
	class kid;
}