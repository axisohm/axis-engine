#pragma once

#include <string>
#include <Windows.h>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <Windows.h>
#include <conio.h>
#include <string>
#include <fstream>
#include <iomanip>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include "BasicDependencies.h"



#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "MSVCRTD.lib")
#pragma comment(lib, "Dwrite")


#define tabMax 255

#define PI_ 3.14159265358979323846


std::string textToScreenSize(std::string);


std::string outStat(std::string, const wchar_t[]);
std::string outStat(std::string, std::string);

int random(int, int);
float randomF(float min = 0, float max = 1);

int getFirstChara(char, std::string);
std::string tabToString(int*, int);
int* stringToTab(std::string);
int stringToInt(std::string);
long stringToLong(std::string);
std::string intToString(int);
std::string boolToString(bool);
std::string longToString(long);

//Tab, value, sizeOfTab
void fillTab(int[], int, int);
//Tab, value, from, to
void fillTabFT(int[], int, int, int);
//Tab, value, from
void fillTabF(int[], int, int);

//value to display, maxValue, amount of characters to display.
std::string statsToBar(int, int, int);

D2D1_RECT_F RECTtoRECTF(RECT);

long countAmountOfOccurences(std::string, std::string);
long countAmountOfOccurences(std::string, char);


#define Xsize 9
// Tab to be filled[][X], tab to fill[X], Y.
void load2Dchar(char*[][Xsize], char[], int);

// Returns the sign of the value.
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}